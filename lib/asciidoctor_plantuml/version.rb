# frozen_string_literal: true

module Asciidoctor
  module PlantUML
    VERSION = '0.0.16'
  end
end
